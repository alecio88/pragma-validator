/*
 * service.js: Defines the web service for the Pinpoint module.
 *
 * (C) 2011 Charlie Robbins
 * MIT LICENSE
 *
 */

var director = require('director');
var traceData = require('./tracedata');


/**
 * Creates the RESTful router for the pinpoint web service
 */
exports.createRouter = function () {
  var router = new director.http.Router().configure({
    strict: false,
    async: true
  });

  router.path(/\/trace\/(\w+)\/gps/, function () {
    //
    // LIST: GET to /trace lists all bookmarks
    //
    this.get(function (id) {
      var gpsTrace = traceData.getGpsData(id+'.txt');
      var result = gpsTrace;

      this.res.json(200, result);
    });

  });

  router.path(/\/trace\/(\w+)\/accelerometer/, function () {
    //
    // LIST: GET to /trace lists all bookmarks
    //
    this.get(function (id) {
      var gpsTrace = traceData.getAccelerometerData(id+'.txt');
      var result = gpsTrace;

      this.res.json(200, result);
    });

  });

  return router;
};
