// traceprocessor.js

var fs = require('fs');
var path = require('path');

function process(filename){
  var filePath = path.join('./data/', filename);
  var array = fs.readFileSync(filePath).toString().split("\n");

  var gpsTrace = [];
  var accelerometerTrace = [];

  for(i in array) {
    var data = array[i].split(';');
    if(data[1] != undefined){
      gps = { time : data[0],
              lat : data[1],
              lng :  data[2]
            };
      accelerometer = [data[3],data[4],data[5]];
      gpsTrace.push(gps);
      accelerometerTrace.push({
        "time" : data[0],
        "x" : accelerometer[0],
        "y" : accelerometer[1],
        "z" : accelerometer[2]
      });

    }
  }

  return {
    "gps" : gpsTrace,
    "accelerometer" : accelerometerTrace
   };
}

exports.getGpsData = function(filename){
  return process(filename).gps;
}

exports.getAccelerometerData = function(filename){
  return process(filename).accelerometer;
}
