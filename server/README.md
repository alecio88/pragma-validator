# nodejs-intro

My introduction presentation to node.js along with sample code at various stages of building a simple RESTful web service with director, cradle, winston, optimist, and http-console.

## Tests

**Tests for this stage of development are left as an exercise to the user.**

#### Author: [Charlie Robbins](http://twitter.com/indexzero)

[0]: http://github.com/flatiron/director
[1]: http://github.com/cloudhead/cradle
[2]: http://github.com/indexzero/winston
[3]: http://github.com/substack/node-optimist
[4]: http://github.com/cloudhead/http-console